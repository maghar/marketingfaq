// arrow go header (right-bottom)
var arrowElem = document.getElementById("up-arrow");
window.onscroll = function() {
  if (window.pageYOffset === 0) {
    arrowElem.classList.add("cls-display-none");
  } else if (window.pageYOffset !== 0) {
    arrowElem.classList.remove("cls-display-none");
  }
};

// focus-blur for slider packages
function onFocusSlide(e){
  var elemFocus = e.parentNode.parentNode.parentNode;
  elemFocus.classList.add('transform-class')
  console.log(elemFocus)
}

function onBlurSlide(e){
  var elemBlur = e.parentNode.parentNode.parentNode
  elemBlur.classList.remove('transform-class')
  console.log(elemBlur)
}


var mmhb = document.getElementById('mobile-menu-header-btn');

mmhb.onclick = function(e){
  console.log(document.getElementById('mobile-menu-header-btn'))
  if(document.getElementById('mobile-menu-header-btn').classList[2]){
    document.getElementById('mobile-menu-header-btn').children[0].classList.add('cls-display-none')
    document.getElementById('mobile-menu-header-btn').children[1].classList.remove('cls-display-none')
  } else {
    document.getElementById('mobile-menu-header-btn').children[0].classList.remove('cls-display-none')
    document.getElementById('mobile-menu-header-btn').children[1].classList.add('cls-display-none')
  }
};

mmhb.addEventListener("blur", function() {
    document.getElementById('mobile-menu-header-btn').children[0].classList.remove('cls-display-none')
    document.getElementById('mobile-menu-header-btn').children[1].classList.add('cls-display-none')
}, true);


var link = document.getElementsByClassName('same-as-selected-lang')

window.onclick = function(){
  if(link[0].innerText === "EN"){
    window.location.href = ('../')
  } else {
    window.location.href = ('/faq/ru')
  }
}